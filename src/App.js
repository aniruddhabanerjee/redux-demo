import React, { Component } from 'react'
import { connect } from 'react-redux'
export class App extends Component {
  render() {
    return (
      <div>
        Counter
        <button onClick={() => this.props.incCounter()}>Increment</button>
        <button onClick={() => this.props.decCounter()}>Decrement</button>
      </div>
    )
  }
}
const mapStateToProp = (state) => {
  return {
    count: state.count,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    incCounter: () => dispatch({ type: 'INCREMENT' }),
    decCounter: () => dispatch({ type: 'DECREMENT' }),
  }
}

export default connect(mapStateToProp, mapDispatchToProps)(App)
